# Klausur / Test am 14.2.2020 um 9:10 Uhr in 4010/4011

1. Fehler finden und auf Papier dokumentieren
2. alles was in Test 1 relevant war
3. selbst etwas ausprogrammieren (GUI, server.js -> copy,paste CSS)
4. SQL. Evtl. einen unbekannten SQL-Befehl anhand einer gegebenen Dokumentatiom selbst erstellen
5. if und else (auch verschachtelt). Bitte auch die alten if-else Sachen anschauen im Trainings-Ordner.
6. Syymetrische  und asymmetrische Verschlüsselung erklären/gegeneinander abgrenzen. Den Sinn jeweils erklären und die Implementation kurz beschreiben( Programme, schlüsselpaar generieren, an Email anheften und anderen schicken können)

## Beispiel zu If-Else:
  ``` Javascript
  // Wenn ein Schüler nicht volljährig ist, wird "Eintritt verweigert".

  // Schülerinnen zahlen 3 Euro.
  // Schüler zahlen 4 Euro.

  var darfHinein = true;
  var istVolljaehrig = 18;
  var geschlecht = "w"

  if(istVolljaehrig){
      darfHinein = true
      console.Log("Die Schülerin darf hinein. ") 
  }else{
      darfHinein = false
     console.Log("Der Schüler darf nicht hinein ") 
  }

  if(geschlecht === "w"){
      darfHinein = true
      console.Log("Die Schülerin darf hinein. Preis: 3 Euro") 
  }else{
      darfHinein = false
     console.Log("Der Schüler darf nicht hinein ") 
  }
  ```
